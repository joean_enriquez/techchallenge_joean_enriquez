/*    ==Scripting Parameters==

    Source Server Version : SQL Server 2016 (13.0.4206)
    Source Database Engine Edition : Microsoft SQL Server Enterprise Edition
    Source Database Engine Type : Standalone SQL Server

    Target Server Version : SQL Server 2014
    Target Database Engine Edition : Microsoft SQL Server Standard Edition
    Target Database Engine Type : Standalone SQL Server
*/

USE [master]
GO
/****** Object:  Database [WhaTechCustBetDB]    Script Date: 11/28/2017 11:42:27 ******/
CREATE DATABASE [WhaTechCustBetDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'WhaTechCustBetDB', FILENAME = N'C:\JOE\test\db\WhaTechCustBetDB.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'BetDB_log', FILENAME = N'C:\JOE\test\db\WhaTechCustBetDB_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [WhaTechCustBetDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [WhaTechCustBetDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [WhaTechCustBetDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [WhaTechCustBetDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [WhaTechCustBetDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [WhaTechCustBetDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET RECOVERY FULL 
GO
ALTER DATABASE [WhaTechCustBetDB] SET  MULTI_USER 
GO
ALTER DATABASE [WhaTechCustBetDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [WhaTechCustBetDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [WhaTechCustBetDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [WhaTechCustBetDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [WhaTechCustBetDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'WhaTechCustBetDB', N'ON'
GO
USE [WhaTechCustBetDB]
GO
/****** Object:  Table [dbo].[Bet]    Script Date: 11/28/2017 11:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bet](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RaceID] [int] NOT NULL,
	[HorseId] [int] NOT NULL,
	[CustomerId] [int] NOT NULL,
	[BetAmt] [money] NOT NULL,
 CONSTRAINT [PK_Bet] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Customer]    Script Date: 11/28/2017 11:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CustomerName] [varchar](250) NOT NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Horse]    Script Date: 11/28/2017 11:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Horse](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[HorseName] [varchar](150) NOT NULL,
	[Stake] [decimal](18, 0) NOT NULL,
	[Win] [decimal](18, 0) NOT NULL,
 CONSTRAINT [PK_Horse] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Race]    Script Date: 11/28/2017 11:42:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Race](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RaceName] [varchar](150) NOT NULL,
	[Status] [varchar](15) NOT NULL,
 CONSTRAINT [PK_Race] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Bet] ON 
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (2, 1, 1, 1, 10.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (3, 1, 1, 2, 20.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (4, 1, 2, 3, 10.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (5, 1, 2, 4, 50.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (6, 1, 3, 10, 500.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (7, 1, 3, 8, 150.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (8, 2, 4, 7, 100.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (9, 2, 5, 4, 30.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (10, 2, 6, 9, 10.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (11, 2, 6, 10, 100.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (13, 3, 11, 5, 400.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (14, 3, 12, 10, 100.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (15, 3, 9, 3, 50.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (16, 3, 10, 4, 20.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (17, 1, 1, 6, 50.0000)
GO
INSERT [dbo].[Bet] ([Id], [RaceID], [HorseId], [CustomerId], [BetAmt]) VALUES (18, 3, 12, 1, 200.0000)
GO
SET IDENTITY_INSERT [dbo].[Bet] OFF
GO
SET IDENTITY_INSERT [dbo].[Customer] ON 
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (1, N'Allan Go')
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (2, N'Peter Pan')
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (3, N'Billy James')
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (4, N'Tam Ngo')
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (5, N'Scott Thomas')
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (6, N'Zarqui Lu')
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (7, N'Victor Vargas')
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (8, N'Matt Hess')
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (9, N'Mark Murphy')
GO
INSERT [dbo].[Customer] ([Id], [CustomerName]) VALUES (10, N'Arnel Eliar')
GO
SET IDENTITY_INSERT [dbo].[Customer] OFF
GO
SET IDENTITY_INSERT [dbo].[Horse] ON 
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (1, N'Horse 1', CAST(1 AS Decimal(18, 0)), CAST(4 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (2, N'Horse 2', CAST(1 AS Decimal(18, 0)), CAST(1 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (3, N'Horse 3', CAST(1 AS Decimal(18, 0)), CAST(5 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (4, N'Horse 100', CAST(1 AS Decimal(18, 0)), CAST(4 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (5, N'Horse 200', CAST(1 AS Decimal(18, 0)), CAST(3 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (6, N'Horse 300', CAST(1 AS Decimal(18, 0)), CAST(7 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (7, N'Horse 400', CAST(1 AS Decimal(18, 0)), CAST(8 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (8, N'Horse 1000', CAST(1 AS Decimal(18, 0)), CAST(9 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (9, N'Horse 2000', CAST(1 AS Decimal(18, 0)), CAST(10 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (10, N'Horse 3000', CAST(1 AS Decimal(18, 0)), CAST(12 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (11, N'Horse 4000', CAST(1 AS Decimal(18, 0)), CAST(14 AS Decimal(18, 0)))
GO
INSERT [dbo].[Horse] ([Id], [HorseName], [Stake], [Win]) VALUES (12, N'Horse 5000', CAST(1 AS Decimal(18, 0)), CAST(2 AS Decimal(18, 0)))
GO
SET IDENTITY_INSERT [dbo].[Horse] OFF
GO
SET IDENTITY_INSERT [dbo].[Race] ON 
GO
INSERT [dbo].[Race] ([Id], [RaceName], [Status]) VALUES (1, N'Race 1', N'Pending')
GO
INSERT [dbo].[Race] ([Id], [RaceName], [Status]) VALUES (2, N'Race 2', N'In Progress')
GO
INSERT [dbo].[Race] ([Id], [RaceName], [Status]) VALUES (3, N'Race 3', N'Completed')
GO
SET IDENTITY_INSERT [dbo].[Race] OFF
GO
ALTER TABLE [dbo].[Horse] ADD  CONSTRAINT [DF_Horse_Odds]  DEFAULT ((0)) FOR [Stake]
GO
USE [master]
GO
ALTER DATABASE [WhaTechCustBetDB] SET  READ_WRITE 
GO
