Solutions Project Structure
1. WhaTechCustomerBets -  this project is for web UI where the race information will display.
2. WhaTechCustomerBetsAPI - this project is for WebAPI where I put diffrent API to use to get information in to the database.
3. WhaTechCustomerBets.Service - this project is for business logic where I put the code retrieving data to the database and manipulate.
4. WhaTechCustomerBets.Model - this for project for ModelView 
5. WhaTechCustomerBets.Data - this file is for database model where I put edmx file

-----------------------------------------------------------------------------------------

How to Run the solutions
1. Run script file dbScript.sql found in db folder to restore the database and data
2. Open the file WhaTechCustomerBets.sln
3. Change the database connection string in web.config and app.config file
4. Please change the URL in javascript file incase the port/url is change(Sorry for inconvenience)
5. Set WhaTechCustomerBets as Set as Start-up project
6. Run the solution if this WebAPI WhaTechCustomerBetsAPI does not run please run. 
7. Please see the document Testing Documents.docx to see how to run different API.

