﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WhaTechCustomerBets.Model;
using WhaTechCustomerBets.Service;
using WhaTechCustomerBetsAPI.Models;

namespace WhaTechCustomerBetsAPI.Controllers
{
    public class GetRiskCustomersController : ApiController
    {
        public IEnumerable<RiskCustomerModel> Get()
        {
            var service = new CustomerService();
            return service.GetRiskCustomer()
                .Select(p => new RiskCustomerModel()
                {
                    CustomerId = p.CustomerId,
                    CustomerName = p.CustomerName,
                    BetAmt = p.BetAmt
                });
        }
        public IEnumerable<RiskCustomerModel> Get(int id)
        {
            var service = new CustomerService();
            return service.GetRiskCustomer(id)
                .Select(p => new RiskCustomerModel()
                {
                    CustomerId = p.CustomerId,
                    CustomerName = p.CustomerName,
                    BetAmt = p.BetAmt
                });
        }

        public IEnumerable<RiskCustomerModel> Get(string name)
        {
            var service = new CustomerService();
            return service.GetRiskCustomer(name)
               .Select(p => new RiskCustomerModel()
               {
                   CustomerId = p.CustomerId,
                   CustomerName = p.CustomerName,
                   BetAmt = p.BetAmt
               });
        }
    }
}
