﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WhaTechCustomerBets.Data.Data;
using WhaTechCustomerBets.Model;
using WhaTechCustomerBets.Service;

namespace WhaTechCustomerBetsAPI.Controllers
{
    public class GetHorsesController : ApiController
    {
        public IEnumerable<HorseModel> Get()
        {
            var service = new HorseService();
            return service.GetHorse();
        }

        public HorseModel Get(int horseId)
        {
            var service = new HorseService();
            return service.GetHorse(horseId);
        }
        public HorseModel Gets(string name)
        {
            var service = new HorseService();
            return service.GetHorse(name);
        }
    }
}
