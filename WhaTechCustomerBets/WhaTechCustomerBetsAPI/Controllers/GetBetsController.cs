﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WhaTechCustomerBets.Service;

namespace WhaTechCustomerBetsAPI.Controllers
{
    public class GetBetsController : ApiController
    {
        public decimal Get()
        {
            var service = new BetService();
            return service.GetAllBet();
        }

        public decimal Get(int id)
        {
            var service = new BetService();
            return service.GetCustomerAllBetAmtPerRace(id);
        }

        public decimal Get(string name)
        {
            var service = new BetService();
            return service.GetCustomerAllBetAmtPerRace(name);
        }
    }
}
