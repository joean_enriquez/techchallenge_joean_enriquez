﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WhaTechCustomerBets.Model;
using WhaTechCustomerBets.Service;

namespace WhaTechCustomerBetsAPI.Controllers
{
    public class GetCustomerAllBetController : ApiController
    {
        public CustomerBetModel Get(int id)
        {
            var service = new CustomerService();
            return service.GetCustomerAllBet(id);
        }

        public IEnumerable<CustomerBetModel> Get(string name)
        {
            var service = new CustomerService();
            return service.GetCustomerAllBet(name);
        }
    }
}
