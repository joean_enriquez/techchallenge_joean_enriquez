﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WhaTechCustomerBets.Model;
using WhaTechCustomerBets.Service;

namespace WhaTechCustomerBetsAPI.Controllers
{
    public class GetCustomersController : ApiController
    {
        public IEnumerable<CustomerModel> Get()
        {
            var service = new CustomerService();
            return service.GetCustomer();
        }


    }
}
