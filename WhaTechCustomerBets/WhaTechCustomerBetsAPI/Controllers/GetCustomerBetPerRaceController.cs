﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WhaTechCustomerBets.Model;
using WhaTechCustomerBets.Service;

namespace WhaTechCustomerBetsAPI.Controllers
{
    public class GetCustomerBetPerRaceController : ApiController
    {
        public CustomerRaceBetModel Get(int raceid, int customerid)
        {
            var service = new CustomerService();
            return service.GetCustomerBetPerRace(raceid, customerid);
        }

        public IEnumerable<CustomerRaceBetModel> Get(string raceName, string customerName)
        {
            var service = new CustomerService();
            return service.GetCustomerBetPerRace(raceName, customerName);
        }
    }
}
