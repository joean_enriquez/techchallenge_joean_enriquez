﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WhaTechCustomerBets.Data.Data;
using WhaTechCustomerBets.Service;
using WhaTechCustomerBets.Model;

namespace WhaTechCustomerBetsAPI.Controllers
{
    public class GetRacesController : ApiController
    {
        // GET: api/Race
        public IEnumerable<RaceModel> Get()
        {
            var raceService = new RaceService();
            return raceService.GetRace();
        }

        // GET: api/Race/5
        public RaceModel Get(int id)
        {
            var raceService = new RaceService();
            return raceService.GetRace(id);
        }

        public RaceModel Get(string name)
        {
            var raceService = new RaceService();
            return raceService.GetRace(name);
        }

        // POST: api/Product
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Product/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Product/5
        public void Delete(int id)
        {
        }
    }
}
