﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WhaTechCustomerBetsAPI.Models
{
    public class RiskCustomerModel
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public decimal BetAmt { get; set; }

    }
}