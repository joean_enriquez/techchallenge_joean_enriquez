﻿var WhaTechCustomerBetsApp = angular.module("WhaTechCustomerBetsApp", []);

WhaTechCustomerBetsApp.controller('RaceController', function ($scope, $http) {

    $http.get('http://localhost:63242/api/GetRaces').then(function (response) {
        $scope.races = response.data;
    });
});
