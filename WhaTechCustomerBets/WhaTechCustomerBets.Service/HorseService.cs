﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhaTechCustomerBets.Data.Data;
using WhaTechCustomerBets.Model;

namespace WhaTechCustomerBets.Service
{
    public class HorseService
    {
        public IEnumerable<HorseModel> GetHorse()
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                var bets = db.Bets.ToList();
                var horseBets = (from horse in db.Horses.ToList()
                                 select new HorseModel()
                                 {
                                     HorseId = horse.Id,
                                     HorseName = horse.HorseName,
                                     PayOut = (bets.Where(p => p.HorseId == horse.Id).Sum(p => p.BetAmt) / horse.Stake) * horse.Win,
                                     BetCount = bets.Where(p => p.HorseId == horse.Id).Count()
                                 }).ToList();


                return horseBets;
            }
        }

        public HorseModel GetHorse(int horseId)
        {
            var horses = GetHorse();
            return horses.Where(p => p.HorseId == horseId).FirstOrDefault();
        }
        public HorseModel GetHorse(string name)
        {
            var horses = GetHorse();
            return horses.Where(p => p.HorseName == name).FirstOrDefault();
        }
    }
}
