﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhaTechCustomerBets.Data.Data;
using WhaTechCustomerBets.Model;

namespace WhaTechCustomerBets.Service
{
    public class CustomerService
    {
        public IEnumerable<CustomerModel> GetCustomer()
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                var customers = (from cust in db.Customers
                                 orderby cust.CustomerName
                                 select new CustomerModel()
                                 {
                                     Id = cust.Id,
                                     CustomerName = cust.CustomerName,
                                 }).ToList();


                return customers;
            }
        }

        public CustomerBetModel GetCustomerAllBet(int id)
        {
            var customers = GetCustomerAllBet();
            return customers.Where(p => p.Id == id).FirstOrDefault();
        }

        public IEnumerable<CustomerBetModel> GetCustomerAllBet(string name)
        {
            var customers = GetCustomerAllBet();
            return customers.Where(p => p.CustomerName == name);
        }

        public IEnumerable<CustomerBetModel> GetCustomerAllBet()
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                var bets = db.Bets.ToList();
                var customers = (from cust in db.Customers.ToList()
                                 select new CustomerBetModel()
                                 {
                                     Id = cust.Id,
                                     CustomerName = cust.CustomerName,
                                     BetAmt = bets.Where(p => p.CustomerId == cust.Id).Sum(p => p.BetAmt)
                                 });


                return customers;
            }
        }

        public CustomerRaceBetModel GetCustomerBetPerRace(int raceId, int customerId)
        {
            var customers = GetCustomerBetPerRace();
            return customers.Where(p => p.CustomerId == customerId && p.RaceId == raceId).FirstOrDefault();
        }

        public IEnumerable<CustomerRaceBetModel> GetCustomerBetPerRace(string raceName, string customerName)
        {
            var customers = GetCustomerBetPerRace();
            return customers.Where(p => p.RaceName == raceName && p.CustomerName == customerName);
        }

        public IEnumerable<CustomerRaceBetModel> GetCustomerBetPerRace()
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                var bets = db.Bets.ToList();

                var customerInRaces = (from race in db.Races
                                    join bet in db.Bets on race.Id equals bet.RaceID
                                    join cust in db.Customers on bet.CustomerId equals cust.Id
                                    select new CustomerRaceModel()
                                    {
                                        CustomerId = cust.Id,
                                        CustomerName = cust.CustomerName,
                                        RaceId = race.Id,
                                        RaceName = race.RaceName
                                    })
                                    .Distinct()
                                    .ToList();

               var customerBets = (from cust in customerInRaces
                             select new CustomerRaceBetModel()
                             {
                                 CustomerId = cust.CustomerId,
                                 CustomerName = cust.CustomerName,
                                 RaceId = cust.RaceId,
                                 RaceName = cust.RaceName,
                                 BetAmt = bets.Where(p => p.CustomerId == cust.CustomerId && p.RaceID == cust.RaceId).Sum(p => p.BetAmt),
                             }).ToList();
                return customerBets;
            }
        }

        public IEnumerable<CustomerRaceBetModel> GetRiskCustomer(int raceId)
        {
            var customers = GetRiskCustomer();
            return customers.Where(p => p.RaceId == raceId);
        }

        public IEnumerable<CustomerRaceBetModel> GetRiskCustomer(string raceName)
        {
            var customers = GetRiskCustomer();
            return customers.Where(p => p.RaceName == raceName);
        }

        public IEnumerable<CustomerRaceBetModel> GetRiskCustomer()
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                var bets = db.Bets.ToList();

                var customerInRaces = (from race in db.Races
                                       join bet in db.Bets on race.Id equals bet.RaceID
                                       join cust in db.Customers on bet.CustomerId equals cust.Id
                                       select new CustomerRaceModel()
                                       {
                                           CustomerId = cust.Id,
                                           CustomerName = cust.CustomerName,
                                           RaceId = race.Id,
                                           RaceName = race.RaceName
                                       })
                                    .Distinct()
                                    .ToList();

                var customerBets = (from cust in customerInRaces
                                    select new CustomerRaceBetModel()
                                    {
                                        CustomerId = cust.CustomerId,
                                        CustomerName = cust.CustomerName,
                                        RaceId = cust.RaceId,
                                        RaceName = cust.RaceName,
                                        BetAmt = bets.Where(p => p.CustomerId == cust.CustomerId && p.RaceID == cust.RaceId).Sum(p => p.BetAmt),
                                    })
                                    .Where(p => p.BetAmt > 200)
                                    .ToList();
                return customerBets;
            }
        }
    }
}
