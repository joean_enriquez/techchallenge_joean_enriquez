﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhaTechCustomerBets.Data.Data;

namespace WhaTechCustomerBets.Service
{
    public class BetService
    {
        public decimal GetAllBet()
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                return db.Bets.Sum(p => p.BetAmt);
            }
        }

        public decimal GetCustomerAllBetAmtPerRace(int raceId)
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                return db.Bets.Where(p => p.RaceID == raceId).Sum(p => p.BetAmt);
            }
        }

        public decimal GetCustomerAllBetAmtPerRace(string raceName)
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                var raceIds = db.Races.Where(p => p.RaceName == raceName).Select(p => p.Id).ToList();
                return db.Bets.Where(p => raceIds.Contains(p.RaceID)).Sum(p => p.BetAmt);
            }
        }
    }
}
