﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WhaTechCustomerBets.Data.Data;
using WhaTechCustomerBets.Model;

namespace WhaTechCustomerBets.Service
{
    public class RaceService
    {
        public IEnumerable<RaceModel> GetRace()
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                var bets = db.Bets.ToList();

                var horseInRaces = (from race in db.Races
                                    join bet in db.Bets on race.Id equals bet.RaceID
                                    join horse in db.Horses on bet.HorseId equals horse.Id
                                    select new HorseRaceModel()
                                    {
                                        HorseId = horse.Id,
                                        HorseName = horse.HorseName,
                                        RaceId = race.Id,
                                        RaceName = race.RaceName,
                                        Stake = horse.Stake,
                                        Win = horse.Win
                                    })
                                    .Distinct()
                                    .ToList();

                var horseBets = (from hr in horseInRaces
                                 select new HorseRaceBetModel()
                                 {
                                     HorseId = hr.HorseId,
                                     HorseName = hr.HorseName,
                                     RaceId = hr.RaceId,
                                     PayOut = (bets.Where(p => p.HorseId == hr.HorseId).Sum(p => p.BetAmt) / hr.Stake) * hr.Win,
                                     BetCount = bets.Where(p => p.HorseId == hr.HorseId).Count(),
                                 }).ToList();

                var races = (from race in db.Races.ToList()
                             select new RaceModel()
                             {
                                 Id = race.Id,
                                 RaceName = race.RaceName,
                                 Status = race.Status,
                                 TotalBet = bets.Where(p => p.RaceID == race.Id).Sum(p => p.BetAmt),
                                 Horses = horseBets.Where(p => p.RaceId == race.Id)
                             }).ToList();
                return races;
            }
        }

        public RaceModel GetRace(int id)
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                var bets = db.Bets.ToList();

                var horseInRaces = (from race in db.Races
                                    join bet in db.Bets on race.Id equals bet.RaceID
                                    join horse in db.Horses on bet.HorseId equals horse.Id
                                    select new HorseRaceModel()
                                    {
                                        HorseId = horse.Id,
                                        HorseName = horse.HorseName,
                                        RaceId = race.Id,
                                        RaceName = race.RaceName,
                                        Stake = horse.Stake,
                                        Win = horse.Win
                                    })
                                    .Distinct()
                                    .ToList();

                var horseBets = (from hr in horseInRaces
                                 select new HorseRaceBetModel()
                                 {
                                     HorseId = hr.HorseId,
                                     HorseName = hr.HorseName,
                                     RaceId = hr.RaceId,
                                     PayOut = (bets.Where(p => p.HorseId == hr.HorseId).Sum(p => p.BetAmt) / hr.Stake) * hr.Win,
                                     BetCount = bets.Where(p => p.HorseId == hr.HorseId).Count(),
                                 }).ToList();

                var races = (from race in db.Races.ToList()
                             join bet in bets on race.Id equals bet.RaceID
                             where race.Id == id
                             select new RaceModel()
                             {
                                 Id = race.Id,
                                 RaceName = race.RaceName,
                                 Status = race.Status,
                                 TotalBet = bets.Where(p => p.RaceID == race.Id).Sum(p => p.BetAmt),
                                 Horses = horseBets.Where(p => p.RaceId == race.Id)
                             }).FirstOrDefault();
                return races;
            }
        }

        public RaceModel GetRace(string name)
        {
            using (var db = new WhaTechCustBetDBEntities())
            {
                var horseBets = (from horse in db.Horses
                                 join bet in db.Bets on horse.Id equals bet.HorseId
                                 select new HorseBetModel()
                                 {
                                     HorseId = horse.Id,
                                     HorseName = horse.HorseName,
                                     CustomerId = bet.CustomerId,
                                     BetAmt = bet.BetAmt,
                                     Stake = horse.Stake,
                                     Win = horse.Win

                                 }).ToList();

                var races = (from race in db.Races.ToList()
                             where race.RaceName == name
                             select new RaceModel()
                             {
                                 Id = race.Id,
                                 RaceName = race.RaceName,
                                 Status = race.Status,
                                 TotalBet = horseBets.Where(p => p.RaceId == race.Id).Sum(p => p.BetAmt)
                             }).FirstOrDefault();
                return races;
            }
        }
    }
}
