﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhaTechCustomerBets.Model
{
    public class HorseRaceModel
    {
        public int HorseId { get; set; }
        public string HorseName { get; set; }
        public int RaceId { get; set; }
        public string RaceName { get; set; }
        public decimal Stake { get; set; }
        public decimal Win { get; set; }
    }
}
