﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhaTechCustomerBets.Model
{
    public class HorseRaceBetModel
    {
        public int HorseId { get; set; }
        public string HorseName { get; set; }
        public int RaceId { get; set; }
        public int BetCount { get; set; }
        public decimal PayOut { get; set; }
    }
}
