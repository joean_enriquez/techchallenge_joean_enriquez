﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhaTechCustomerBets.Model
{
    public class CustomerRaceModel
    {
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int RaceId { get; set; }
        public string RaceName { get; set; }
    }
}
