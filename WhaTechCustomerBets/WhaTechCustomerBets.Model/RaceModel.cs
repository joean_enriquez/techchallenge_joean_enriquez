﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhaTechCustomerBets.Model
{
    public class RaceModel
    {
        public int Id { get; set; }
        public string RaceName { get; set; }
        public string Status { get; set; }

        public decimal TotalBet { get; set; }

        public IEnumerable<HorseRaceBetModel> Horses { get; set; }
    }
}
