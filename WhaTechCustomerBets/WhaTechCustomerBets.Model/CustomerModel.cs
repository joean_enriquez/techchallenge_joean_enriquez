﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhaTechCustomerBets.Model
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
    }
}
