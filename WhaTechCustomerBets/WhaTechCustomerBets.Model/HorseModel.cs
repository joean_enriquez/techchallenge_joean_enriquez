﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WhaTechCustomerBets.Model
{
    public class HorseModel
    {
        public int HorseId { get; set; }
        public string HorseName { get; set; }
        public int BetCount { get; set; }
        public decimal PayOut { get; set; }
    }
}
